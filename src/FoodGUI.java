import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

import static javax.swing.JOptionPane.showMessageDialog;

public class FoodGUI {
    private JPanel root;
    private JButton tunaButton;
    private JButton samonButton;
    private JButton hamatiButton;
    private JTextArea textArea1;
    private JButton ikaButton;
    private JButton ikuraButton;
    private JButton tamagoButton;
    private JLabel textArea2;
    private JButton checkOutButton;
    private JLabel textArea3;

    public String[] order(String name, int cost) {
        String[] kosuu = {"1", "2", "3", "4"};

        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order " + name + "?",
                "Order Comfirmation",
                JOptionPane.YES_NO_OPTION
        );

        int sara = 0;

        if(confirmation==0) {
           //何皿注文するか
            sara = JOptionPane.showOptionDialog(
                    null,
                    "How many plates would you like to order?",
                    "Select plates",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    kosuu, kosuu[1] //これ以上増やすとエラー、実行結果は正しく出る
            );

            int kettei = JOptionPane.showConfirmDialog(
                    null,
                    name + " " + kosuu[sara] + "plate" + " Ordered.",
                    "Check select things.",
                    JOptionPane.OK_CANCEL_OPTION
            );

            if (kettei == 0) {
                int price = Plateprice(sara, cost);
                String JoptionPane;
                showMessageDialog(null, "Thank you for ordering " +name + "!.It will be served as soon as possible.");
                return new String[]{kosuu[sara], String.valueOf(price)};
            }
        }
        return null;
    }


    public int Plateprice(int plate, int price){
        int[] zougaku = {1*price,2*price,3*price,4*price};
        return zougaku[plate];
    }



    public int keisan(int cost){
        int goukei = Integer.parseInt(textArea2.getText());
        int tax = (cost/10);

        return goukei + cost + tax;
    }


    public void tyuumon(String name,int cost) {

        String[] Tyuumon = order(name, cost);
        if(Tyuumon == null) return; //注文するかどうか

        String numberplate = Tyuumon[0];
        int nedan = Integer.parseInt(Tyuumon[1]);

        int zeikin = (keisan(nedan)/11) ;//税金表示
        textArea3.setText(String.valueOf(zeikin));

        //注文状況
        String currentText = textArea1.getText();
        textArea1.setText(currentText + name + " " +numberplate +"plate "+nedan+"yen\n");

        //合計金額
        textArea2.setText(String.valueOf(keisan(nedan)));

    }


   public void checkout() {

       int confirmation = JOptionPane.showConfirmDialog(
               null,
               "Would you like to check out?  ",
               "Order Comfirmation",
               JOptionPane.YES_NO_OPTION
       );

       if (confirmation == 0) {
           int Checkout = JOptionPane.showConfirmDialog(
                   null,
                   "Thank you. The total price is " + keisan(0) + " yen.",
                   "Order Received",
                   JOptionPane.OK_CANCEL_OPTION
           );

           if (Checkout == 0) {

               textArea1.setText(null);
               textArea2.setText("0");
               textArea3.setText("0");
           }
       }
   }


    public FoodGUI() {
        textArea2.setText("0");
        textArea3.setText("0");

        tunaButton.setIcon(new javax.swing.ImageIcon(".\\src\\マグロ２.jpg"));
        tunaButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {


            tyuumon("maguro",150);
           }
        }
    );

        samonButton.setIcon(new javax.swing.ImageIcon(".\\src\\サーモン.jpg"));
        samonButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {

              tyuumon("salmon",120);
        }
    }
    );

        hamatiButton.setIcon(new javax.swing.ImageIcon(".\\src\\活〆とろはまち.jpg"));
        hamatiButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {


            tyuumon("hamachi",140);
        }
    }
    );

        ikaButton.setIcon(new javax.swing.ImageIcon(".\\src\\いか.jpg"));
        ikaButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {

            tyuumon("ika", 130);
        }
    }
    );

        ikuraButton.setIcon(new javax.swing.ImageIcon(".\\src\\いくら.jpg"));
        ikuraButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {

            tyuumon("ikura",200);
        }
    }
    );

        tamagoButton.setIcon(new javax.swing.ImageIcon(".\\src\\厚切り玉子.jpg"));
        tamagoButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {

            tyuumon("tamago",100);
        }
    }
    );

        checkOutButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {

            checkout();
        }
    }
    );

    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodGUI");
        frame.setContentPane(new FoodGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
